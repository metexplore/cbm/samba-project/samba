import argparse
import sys

import pandas as pd
import os
from pathlib import Path
import re


def main(args):
    description = "Script for exporting KO ID scenarios from a metabolic network."

    parser = argparse.ArgumentParser(description=description, formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("-d", "--dictout", help="Dict file path and name: path/to/folder/scenario_dict.tsv")
    parser.add_argument("-c", "--config", help="Whether the input file is a provided KO file or generated scenarios.")
    parser.add_argument("-i", "--input", help="Input file containing gene or reaction IDs to KO. To KO several at "
                                              "once, add IDs separated by spaces on the same row. To KO different "
                                              "scenarios, add them on new lines.")

    args = parser.parse_args(args)
    input_file = args.input
    config = args.config
    if config == "simple":
        ids_in = pd.read_csv(input_file, sep="\t")
        prefix = "scenario"
    elif config == "scenario":
        scenarios = []
        ids = []
        prefix = re.sub(r'\d+', '', Path(input_file).stem)
        filepath = os.path.join(os.path.sep, os.getcwd(), os.path.dirname(input_file))
        allfiles = [os.path.join(dirpath, f) for (dirpath, dirnames, filenames) in os.walk(filepath) for f in filenames]
        for filename in allfiles:
            with open(filename) as f:
                ids.append(f.read())
            scenarios.append(Path(filename).stem)
        ids_in = pd.DataFrame(zip(scenarios, ids), columns=["Scenario", "IDs"])

    # Make a df with scenario names as "scenario_X" to be used as filenames
    scenario_ids = pd.DataFrame({"Scenario": ids_in["Scenario"]})
    scenario_ids["Filename"] = [prefix + "_" + str(i + 1) for i in scenario_ids.index.values]
    # Write files
    scenario_ids.to_csv(args.dictout, sep="\t", index=False)


if __name__ == '__main__':
    main(sys.argv[1:])

import argparse
import random
import cobra

def export_ko_ids(model, each, scale="pathway", scenario="singlerxn"):
    """

    :param model: A model imported using cobrapy
    :param scale: choose from [pathway, network] to KO within pathways or the entire network
    :param scenario: choose from [singlerxn, allrxn] to KO one random reaction or all reactions in <scale>
    :param each: set each to True to loop over <scale> to generate <scenario> KO IDs
    :return: a list of reaction IDs
    """
    if scale == "network" and scenario == "allrxn":
        raise ValueError("Combination of allrxn and network is impossible. KOing every reaction "
                         "in the network simultaneously is not feasible.")

    ko_ids = {}
    if scenario == "singlerxn":
        if scale == "pathway":
            if each:
                for g in model.groups:
                    ko_ids[g.id] = []
                    # For each pathway, get one random reaction to KO
                    ko_ids[g.id].append(random.choice(tuple(g.members)).id)
            else:
                # Choose one random pathway in which to KO one random reaction
                pathway = random.choice(tuple(model.groups))
                ko_ids[pathway.id] = ([random.choice(tuple(pathway.members)).id])
        if scale == "network":
            if each:
                # KO every single reaction in the network independently
                ko_ids[model.id] = []
                for r in model.reactions:
                    ko_ids[model.id].append([r.id])
            else:
                # Get only one random reaction from the entire network
                rxn = random.choice(tuple(model.reactions))
                ko_ids[model.id] = ([rxn.id])
    if scenario == "allrxn" and scale == "pathway":
        if each:
            for g in model.groups:
                # For each pathway, KO all reactions at once
                ko_ids[g.id] = ([r.id for r in g.members])
        else:
            # Choose a random pathway in which to KO all reactions
            pathway = random.choice(tuple(model.groups))
            ko_ids[pathway.id] = ([r.id for r in pathway.members])
    return ko_ids




if __name__ == '__main__':
    description = "Script for exporting KO ID scenarios from a metabolic network."

    parser = argparse.ArgumentParser(description=description, formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("-m", "--model", help="Metabolic model. Supported file format: SBML")
    parser.add_argument("-o", "--outpath", help="Outfile path (without filename)")
    parser.add_argument("-s", "--scale", help="KO [scenario] over [each] pathway or network", choices=["pathway", "network"], default="pathway")
    parser.add_argument("-c", "--scenario", help="KO singlerxn or allrxn over [each] [scale]", choices=["singlerxn", "allrxn"], default="singlerxn")
    parser.add_argument("-e", "--each",action='store_true', help="KO [scenario] over each or all [scale]")
    
    args = parser.parse_args()
    model = cobra.io.read_sbml_model(args.model)
    ko_ids = export_ko_ids(model, scale=args.scale, scenario=args.scenario, each=args.each)
    first_group = next(iter(ko_ids))
    if args.scenario == "pathway" and (len(ko_ids[first_group]) == len(model.reactions) or len(ko_ids[first_group]) == 0):
        raise Exception("This model's GROUP formatting is incorrect, check your model file.")

    if len(ko_ids) == 1:  # When knocking out every reaction in the model, ko_ids is a dict size 1
        for r in ko_ids[first_group]:
            with open(args.outpath+r[0]+".txt", "w") as f:
                f.write('%s\t0\n' % r[0])

    else:
        for key in ko_ids:
            with open(args.outpath+key+".txt", "w") as f:
                if len(ko_ids[key]) == 1:
                    f.write(ko_ids[key][0]+"\t"+str(0))
                else:
                    f.write(" ".join(ko_ids[key])+"\t"+str(0)+"\n")


# numpy needs to be version 1.23.5 or below to work with cobrapy
# conda install numpy=1.23

import argparse
import pandas as pd
from samba.fva.fva_functions import calculate_score
from pathlib import Path
import re

if __name__ == '__main__':
    description = "Script for exporting KO ID scenarios from a metabolic network."

    parser = argparse.ArgumentParser(description=description, formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("-o", "--outpath", help="Outfile filepath and name for the scores file")
    parser.add_argument("-b", "--boundsoutpath", help="Outfile filepath and name for the bounds file")
    parser.add_argument("-w", "--wtin", help="Input file for WT")
    parser.add_argument("-k", "--koin", help="Input file for KO")

    args = parser.parse_args()
    wtinpath = args.wtin
    koinpath = args.koin

    wt = pd.read_csv(wtinpath, header=0, names=["Metab", "minWT", "maxWT"])
    ko = pd.read_csv(koinpath, header=0, names=["Metab", "minKO", "maxKO"])
    scenario_file = Path(koinpath).with_suffix('').stem
    scenario = re.findall(r'.*__FVA', scenario_file)[0][:-5]
    wt_ko_merged = ko.merge(wt, on="Metab", how="left").set_index("Metab")
    
    # Calculate score
    score_dict, flux_change = calculate_score(wt_ko_merged)
    pd.DataFrame.from_dict(flux_change, orient="index", columns=[scenario]).rename_axis("Metab").reset_index().to_csv(args.outpath, index=False)
    
    # Write bounds file
    wt_ko_long = wt_ko_merged.melt(ignore_index=False, value_name=scenario, var_name="bound").reset_index()
    wt_ko_long.to_csv(args.boundsoutpath, index=False)

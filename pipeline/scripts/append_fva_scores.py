import argparse
import pandas as pd
import os
from pathlib import Path

if __name__ == '__main__':
    description = "Script for exporting KO ID scenarios from a metabolic network."

    parser = argparse.ArgumentParser(description=description, formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("-o", "--outpath", help="Outfile filepath and name for the scores file")
    parser.add_argument("-b", "--boundsoutpath", help="Outfile filepath and name for the bounds file")
    parser.add_argument("-i", "--input", help="Input folder with end slash")

    args = parser.parse_args()
    input_dir = args.input

    df_scores = None
    df_bounds = None
    lengths = []

    for f in os.listdir(input_dir):
        if Path(f).with_suffix('').stem.endswith("scores"):
            if df_scores is None:
                df_scores = pd.read_csv(input_dir+f)
                lengths.append(len(df_scores))
                # df_scores["Metab"] = df_scores["Metab"].astype(str)
            else:
                df_scores = pd.merge(df_scores, pd.read_csv(input_dir+f), how="left", on="Metab")
    max_length = list(set(lengths))[0]
    missing = []
    for f in os.listdir(input_dir):
        if Path(f).with_suffix('').stem.endswith("bounds"):
            # First run
            if df_bounds is None:
                df_in = pd.read_csv(input_dir+f)
                # If the first file read is empty, save it for later
                if len(df_in) < max_length:
                    missing.append(f)
                    next
                else:
                    df_bounds = df_in
            else:
                df_in = pd.read_csv(input_dir+f)
                if len(df_in) < max_length:
                    df_bounds = pd.merge(df_bounds, df_in.reindex(range(0, max_length*4)), how="left", on=["Metab", "bound"])
                else:
                    df_bounds = pd.merge(df_bounds, df_in, how="left", on=["Metab", "bound"])
    # Add missing empties
    for f in missing:
        df_in = pd.read_csv(input_dir+f)
        df_bounds = pd.merge(df_bounds, df_in.reindex(range(0, max_length*4)), how="left", on=["Metab", "bound"])

    df_scores.to_csv(args.outpath, index=False, sep="\t")
    df_bounds.to_csv(args.boundsoutpath, index=False, sep="\t")

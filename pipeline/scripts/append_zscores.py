import pandas as pd
import argparse
import sys


def main(args):
    description = "Script for calculating zscores from two sampling results"

    parser = argparse.ArgumentParser(description=description, formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("-i", "--input", help="File paths to all zscore files", nargs="+")
    parser.add_argument("-o", "--outpath", help="Outfile path (with filename): path/to/folder/file.tsv")
    parser.add_argument("-l", "--live", action="store_true", help="Use this if running the script in a live notebook or environment.")

    args = parser.parse_args(args)
    all_files = []
    if args.live:
        input_files = [x for xs in args.input for x in xs]
    else:
        input_files = args.input
    for f in input_files:
        try:
            df = pd.read_csv(f, sep="\t", header=0, index_col="Metab")
            all_files.append(df)
        except pd.errors.EmptyDataError:
            print(f + " is empty. Ignoring it when appending...")

    all_files_df = pd.concat(all_files, axis=1)
    all_files_df.to_csv(args.outpath, sep="\t")


if __name__ == '__main__':
    main(sys.argv[1:])
import argparse
import sys

import cobra
import pandas as pd
import math


def prepare_ids(model, ids):
    """

    :param model: A model imported using cobrapy
    :param ids: ID df of scenarios and IDs (genes or reactions).
    :return: a df of scenarios and reaction IDs.
    """
    # Check IDs are in the model
    for index, row in ids.iterrows():
        for sc_id in row["IDs"]:
            try:
                test_obj = model.reactions.get_by_id(sc_id)
            except KeyError:
                try:
                    test_obj = model.genes.get_by_id(sc_id)
                except KeyError:
                    raise ValueError("The ID "+sc_id+" is not a gene or reaction ID for the provided model.")
    # Detect if genes or reactions:
    out_ids = []
    if test_obj in model.reactions:
        print("Detected reaction IDs.")
        out_ids = [r for r in ids["IDs"]]
    elif test_obj in model.genes:
        print("Detected gene IDs, converting to reactions via GPR.")
        for index, row in ids.iterrows():
            with model:
                rxns = []
                for r in model.reactions:
                    if not r.gpr.eval(row["IDs"]):
                        rxns.append(r.id)
            out_ids.append(rxns)
    else:
        print("Something strange is going on..")
    out_ids_scenario = pd.DataFrame(zip(ids["Scenario"], out_ids, ids["Reduction"]),
                                    columns=["Scenario", "IDs", "Reduction"])
    return out_ids_scenario


def main(args):
    description = "Script for exporting KO ID scenarios from a metabolic network."

    parser = argparse.ArgumentParser(description=description, formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("-m", "--model", help="Metabolic model. Supported file format: SBML")
    parser.add_argument("-o", "--outpath", help="Outfile path (without filename): path/to/folder/")
    parser.add_argument("-i", "--input", help="Input file containing gene or reaction IDs to KO. To KO several at "
                                              "once, add IDs separated by spaces on the same row. To KO different "
                                              "scenarios, add them on new lines.")
    parser.add_argument("-l", "--live", action="store_true", help="Use this if running the script in a live notebook or environment.")

    args = parser.parse_args(args)
    model_path = args.model
    input_file = args.input
    out_path = args.outpath

    model = cobra.io.read_sbml_model(model_path)

    ids_in = pd.read_csv(input_file, sep="\t")
    if "Reduction" not in ids_in.columns:
        # If reduction isn't provided, fill Reduction with 100% reduction (aka flux set to 0) = default KO
        ids_in["Reduction"] = len(ids_in) * [0]
    scenario_ids = pd.DataFrame({"Scenario": ids_in["Scenario"], "Reduction": ids_in["Reduction"]})
    scenario_ids["Filename"] = ["scenario_"+str(i+1) for i in scenario_ids.index.values]

    if isinstance(ids_in.loc[0, "IDs"], str):
        ids_in["IDs"] = ids_in["IDs"].apply(lambda x: x.split(" "))
        ids = ids_in
        out_ids = prepare_ids(model, ids)
    elif math.isnan(ids_in.loc[0, "IDs"]):
        ids_in["IDs"] = ids_in["IDs"].apply(lambda x: [x])
        out_ids = ids_in
    else:
        raise ValueError("Unsupported input ID format.")

    if len(out_ids) == len(scenario_ids):
        out_ids["Scenario"] = scenario_ids["Filename"]
    else:
        raise ValueError("ID conversion issue.")

    # Write files
    for index, row in out_ids.iterrows():
        with open(out_path+row["Scenario"]+".txt", "w") as f:
            if len(row["IDs"]) == 1:
                f.write(str(row["IDs"][0])+"\t"+str(row["Reduction"]))
            else:
                f.write(" ".join(row["IDs"])+"\t"+str(row["Reduction"])+"\n")
        print("Wrote file: "+out_path+row["Scenario"]+".txt")

    if args.live is not None:
        file_dict = {}
        for index, row in out_ids.iterrows():
            file_dict[row["Scenario"]] = " ".join(row["IDs"])
        return file_dict


if __name__ == '__main__':
    main(sys.argv[1:])

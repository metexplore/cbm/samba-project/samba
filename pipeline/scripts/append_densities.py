import pandas as pd
import argparse
import pickle
import json
import os


if __name__ == '__main__':
    description = "Script for calculating zscores from two sampling results"

    parser = argparse.ArgumentParser(description=description, formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("-i", "--input", help="File paths to all density files", nargs="+")
    parser.add_argument("-o", "--outpath", help="Outfile path (with filename): path/to/folder/file.tsv")

    args = parser.parse_args()

    all_files = {}
    for f in args.input:
        scenario = os.path.basename(f).rstrip("_densities.pkl")
        with open(f, "rb") as openfile:
            try:
                density = pickle.load(openfile)
                all_files[scenario] = density
            except pd.errors.EmptyDataError:
                print(f + " is empty. Ignoring it when appending...")

    with open(args.outpath, "w") as out:
        json.dump(all_files, out)

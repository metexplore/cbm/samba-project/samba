import sys

import pandas as pd
import argparse


def zscore(self):
    return self.mean() / self.std()


def calculate_zscores(df1, df2, n):
    """

    :param df1: Dataframe of the (WT) condition to subtract from df2
    :param df2: Dataframe of the perturbed condition
    :param n: Number of rows to sample
    :return: Dataframe of zscores per input column
    """
    df1_samp = df1.sample(n).reset_index(drop=True)
    df2_samp = df2.sample(n).reset_index(drop=True)

    diff_samp = df2_samp.sub(df1_samp, axis="columns")

    return diff_samp.apply(zscore, axis=0)


def main(args):
    description = "Script for calculating zscores from two sampling results"

    parser = argparse.ArgumentParser(description=description, formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("-i", "--input1", help="File path to the first (WT) sampling result")
    parser.add_argument("-o", "--outpath", help="Outfile path (with filename): path/to/folder/file.tsv")
    parser.add_argument("-j", "--input2", help="File path to the second (MUT) sampling result")
    parser.add_argument("-p", "--prefix", help="Prefix of the sampling pair")
    parser.add_argument("-s", "--samplepercent",
                        help="Percent of the total number of samples to sample when calculating "
                             "zscore, between 0 and 1", type=float, default=1)

    args = parser.parse_args(args)
    # data_path = "/home/juliette/these/pipelines/Human1_mGWAS/Human-GEM_Human1_mGWAS_rerun1_genes_to_KO/"

    # input1 = data_path + "raw_samples/scenario_1__sampling__WT.csv.gz"
    # input2 = data_path + "raw_samples/scenario_1__sampling__KO.csv.gz"
    try:
        df_1 = pd.read_csv(args.input1)
    except pd.errors.EmptyDataError:
        print(args.input1+" is empty. Cannot calculate z-score for "+args.prefix+".")
        df_1 = None
    try:
        df_2 = pd.read_csv(args.input2)
    except pd.errors.EmptyDataError:
        print(args.input2+" is empty. Cannot calculate z-score for "+args.prefix+".")
        df_2 = None

    if df_1 is not None and df_2 is not None:
        zscores = calculate_zscores(df_1, df_2, int(args.samplepercent * len(df_1.index))).reset_index()
        zscores.rename(columns={"index": "Metab", 0: args.prefix}, inplace=True)
    else:
        zscores = pd.DataFrame()

    zscores.to_csv(args.outpath, index=False, sep="\t")


if __name__ == '__main__':
    main(sys.argv[1:])

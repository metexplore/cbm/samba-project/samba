import pandas as pd
import argparse
import pickle
import numpy as np
from scipy.stats import gaussian_kde


if __name__ == '__main__':
    description = "Script for calculating zscores from two sampling results"

    parser = argparse.ArgumentParser(description=description, formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("-i", "--input1", help="File path to the first (WT) sampling result")
    parser.add_argument("-j", "--input2", help="File path to the second (MUT) sampling result")
    parser.add_argument("-o", "--outpath", help="Outfile path (with filename): path/to/folder/file.tsv")
    parser.add_argument("-p", "--prefix", help="Prefix of the sampling pair")

    args = parser.parse_args()

    try:
        df_1 = pd.read_csv(args.input1)
    except pd.errors.EmptyDataError:
        print(args.input1 + " is empty. Cannot calculate density for " + args.prefix + ".")
        df_1 = None
    try:
        df_2 = pd.read_csv(args.input2)
    except pd.errors.EmptyDataError:
        print(args.input2 + " is empty. Cannot calculate density for " + args.prefix + ".")
        df_2 = None
    t = {"WT": {}, "MUT": {}}
    if df_1 is not None and df_2 is not None:
        for m in df_1:
            X_axis_1 = np.linspace(np.min(df_1[m]), np.max(df_1[m]), 512)
            X_axis_2 = np.linspace(np.min(df_2[m]), np.max(df_2[m]), 512)
            try:
                kde_1 = gaussian_kde(df_1[m], bw_method="scott")
                Y_axis_1 = kde_1.pdf(X_axis_1)
            except np.linalg.LinAlgError:
                if X_axis_1.sum() == 0.0:
                    Y_axis_1 = np.ones([512])
            try:
                kde_2 = gaussian_kde(df_2[m], bw_method="scott")
                Y_axis_2 = kde_2.pdf(X_axis_2)
            except np.linalg.LinAlgError:
                if X_axis_2.sum() == 0.0:
                    Y_axis_2 = np.ones([512])
            t["WT"][m] = {"x": X_axis_1.round(5).tolist(), "y": Y_axis_1.round(5).tolist()}
            t["MUT"][m] = {"x": X_axis_2.round(5).tolist(), "y": Y_axis_2.round(5).tolist()}
    else:
        densities = t

    with open(args.outpath, "wb") as f:
        pickle.dump(t, f)
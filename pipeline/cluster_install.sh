#!/bin/bash
#SBATCH -J cluster_install
#SBATCH -o cluster_install_out.out
#SBATCH -e cluster_install_err.out

# CHANGE THESE PATHS
ENVPATH="/home/jcooke/work/these/clean/env"
WORKINGDIR="/home/jcooke/work/these/clean/pipelines/IEM/"

# CHANGE THIS MODULE LOAD TO WHERE YOU LOAD PYTHON
module purge
module load system/Python-3.7.4

python -m venv $ENVPATH/samba-3.7
source $ENVPATH/samba-3.7/bin/activate

pip install --upgrade pip
pip install sambaflux
pip install --upgrade sambaflux

#export PYTHONPATH=${PYTHONPATH}:"/home/jcooke/save/CPLEX/cplex/python/3.7/x86-64_linux"

cd $WORKINGDIR
git clone --depth 1 https://forgemia.inra.fr/metexplore/cbm/samba-project/samba.git
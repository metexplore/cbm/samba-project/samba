#!/bin/bash
#SBATCH -J submit_samba
#SBATCH -o submit_out.out
#SBATCH -e submit_err.out

# CHANGE THIS PATH TO YOUR VENV PATH
ENVPATH="/home/jcooke/work/these/clean/env"

# CHANGE THIS MODULE LOAD TO WHERE YOU LOAD PYTHON
module purge
module load system/Python-3.7.4

source $ENVPATH/samba-3.7/bin/activate

snakemake --profile default
from json import dump
from pathlib import Path
from samba.iopy.read_model import import_model
from samba.setup.prepare_reactions import set_exchanges_rxn_bounds, parse_rxns
from samba.sampling.wtopt import wtopt_r2, optimise_wt
from samba.sampling.sample_functions import sample_time
from samba.iopy.export import write_sampling, extract_results, write_fva, extract_fva_results
from samba.fva.fva_functions import run_fva
import logging
import argparse
import pandas as pd
import time
import sys

MIN_VAL = 0
MAX_VAL = 1


def range_limited_float_type(arg):
    """ Type function for argparse - a float within some predefined bounds """
    try:
        f = float(arg)
    except ValueError:
        raise argparse.ArgumentTypeError("Must be a floating point number")
    if f < MIN_VAL or f > MAX_VAL:
        raise argparse.ArgumentTypeError("Argument must be < " + str(MAX_VAL) + "and > " + str(MIN_VAL))
    return f


def main(args):
    description = "Package for running flux sampling on metabolic models."

    parser = argparse.ArgumentParser(description=description, formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("-m", "--model", help="Metabolic model. Supported file formats: SBML, json, mat.")
    parser.add_argument("-n", "--nsamples", type=int, help="The number of samples")
    parser.add_argument("-p", "--processors", type=int, help="Number of processors")
    parser.add_argument("-t", "--thinning", type=int, required=False, default=100,
                        help="The thinning factor of the generated sampling chain. A thinning of 10 means "
                             "samples are returned every 10 steps.")
    parser.add_argument("-o", "--outpath", help="Outfile path (without filename)")
    parser.add_argument("-k", "--ko", help="KO file containing reactions to KO, "
                                           "specify nothing if you want to sample WT", default=None)
    parser.add_argument("-s", "--sepko", help="Separator between genes/reaction to KO in the KO file.", default=" ")
    parser.add_argument("-c", "--sepkocol", help="Separator between columns in the KO file.", default="\t")
    parser.add_argument("-r", "--results", help="Reactions to output: specify "
                                                "'exchanges' if you want only exchange reactions or "
                                                "'all' for all reactions, or "
                                                "a file for specific reactions.", default="exchanges")
    parser.add_argument("-q", "--quiet", action="store_true", help="Use this flag to silence INFO logging.")
    parser.add_argument("-d", "--debug", action="store_true", help="Use this flag to turn on DEBUG logging.")
    parser.add_argument("--dryrun", action="store_true", help="Use this flag to run the code without running sampling.")
    parser.add_argument("--log", help="Log file path + filename. Set to None to output to console.",
                        default=None)
    parser.add_argument("-b", "--biomass", type=range_limited_float_type,
                        help="Number between 0 and 1, fraction of biomass to optimize", default=0)
    parser.add_argument("--solver", help="Solver to use", choices=["cplex", "gurobi", "glpk"], default="cplex")
    parser.add_argument("--exchangemin", default=10,
                        help="The value used to set the minimum flux through exchange reactions (will be negative). "
                             "Set to None if you want the default exchange reaction bounds.")
    parser.add_argument("--biomassrxn", help="A .tsv containing a Model column with the name of models and a Biomass "
                                             "column with the name of the corresponding biomass reaction.",
                        default=None)
    parser.add_argument("-f", "--fva", action="store_true", help="Use this flag to run FVA as well as sampling.")
    parser.add_argument("--onlyfva", action="store_true", help="Use this flag to run FVA instead of sampling.")
    parser.add_argument("--scenario", help="Force the KO reactions to have non zero flux (wt) or KO the "
                                           "input reactions (ko)", choices=["wt", "ko"], default="ko")

    args = parser.parse_args(args)

    # Write parameter config file
    config_dict = args.__dict__
    with open(args.outpath + 'config.json', 'w') as config_file:
        dump(config_dict, config_file, indent=4)

    # Logging setup
    QUIET = args.quiet
    if args.log is not None:
        logfile_path = args.log
    else:
        logfile_path = None
    # Set level to only warnings or higher if QUIET is true
    if QUIET:
        level = logging.WARNING
    else:
        if args.debug:
            level = logging.DEBUG
        else:
            level = logging.INFO
    # Assign the INFO level to print
    print = logging.info
    logging.basicConfig(level=level,
                        format='%(levelname)s:%(message)s',
                        filename=logfile_path)
    # For important warnings that will be printed regardless of QUIET
    iprint = logging.warning

    dprint = logging.debug

    model_file = args.model

    # TEST
    # model_file = "/home/juliette/these/data/models/test_samba/Recon-2_from_matlab.xml"
    # model_file = "/home/juliette/these/data/models/raw/Human-GEM-1.11.0/model/Human-GEM.xml"

    model = import_model(model_file)

    if args.biomass != 0:
        if args.biomassrxn is None:
            try:
                biomassrxn = model.biomass_reaction
            except AttributeError():
                iprint("Can't detect biomass reaction, please input a tsv using --biomassrxn.")
        else:
            # biomassrxn = "/home/juliette/these/data/models/biomass_dict.tsv"
            biomass_dict = pd.read_csv(args.biomassrxn, sep="\t")
            brxn_id = biomass_dict['Biomass'].loc[biomass_dict['Model'] == model.id].item()
            if brxn_id is not float('nan'):
                # Check it exists
                try:
                    biomassrxn = model.reactions.get_by_id(brxn_id)
                except ValueError:
                    iprint("The reaction " + brxn_id + " does not exist in " + model.id)
            else:
                if args.biomass != 0:
                    iprint("You are trying to optimise the biomass reaction for "
                           + model.id + ", however the reaction id is nan. Please set the biomass value to 0 or set a "
                                        "valid biomass reaction")
                    exit(1)
    else:
        print("Running with no biomass optimization (biomass = 0).")

    model.solver = args.solver

    eps = 0.05
    # TODO: make as parameter

    if args.exchangemin.isdigit():
        model = set_exchanges_rxn_bounds(model, float(args.exchangemin))

    # TODO: Check that all KO reactions exist in the model before running everything
    # TODO: Check that the output file path exists before running
    # TODO: Check if KO is feasible before running?
    # TODO: Add more complex reduction (reduction per rxn/gene instead of group)

    ids_to_knockout = parse_rxns(args.ko, args.sepko, args.sepkocol)
    if ids_to_knockout is not None and ids_to_knockout[0] == ['nan']:
        ids_to_knockout = None

    # Get reactions of interest for FVA
    if args.results == "exchanges":
        if model.exchanges is not None:
            rxns_of_interest = [rxn.id for rxn in model.exchanges]
        else:
            rxns_of_interest = [col for col in s if col.startswith('EX_')]
    elif args.results == "all":
        rxns_of_interest = [rxn.id for rxn in model.reactions]
    else:
        rxns_of_interest = parse_rxns(args.results)

    model_clean = model.copy()

    # If no KO ids were provided, just do a basic WT sampling
    if ids_to_knockout is None or len(ids_to_knockout) == 0:
        if args.ko is not None:
            fname = Path(args.ko).stem
        else:
            fname = "default"
        if args.biomass != 0:
            biomassrxn.lower_bound = optimise_wt(model, args.biomass)
        if not args.dryrun:
            if args.fva or args.onlyfva:  # FVA
                # Run WT FVA here
                fva_s = run_fva(model, rxns_of_interest, args.processors, args.biomass)
                fva_results = extract_fva_results(fva_s, args.results, model)
                write_fva(fva_results, args.outpath, "", "", "WT", fname)
            if not args.onlyfva:  # Sampling
                s = sample_time(model, args.nsamples, args.processors, args.thinning)
                s_results = extract_results(s, args.results, model)
                write_sampling(s_results, args.outpath, "", "", "WT", fname)
    else:
        # If only one row: use the file name's gene ID
        # print(ids_to_knockout)
        # print(len(ids_to_knockout))

        if len(ids_to_knockout) == 1:
            fname = Path(args.ko).stem
            print(fname)
        # If multiple rows (separate KOs), use row number
        else:
            fname = 0

        # If KO ids were provided, loop over them and sample the WT then the KO
        for rxn_group in ids_to_knockout:
            if isinstance(fname, int):
                fname += 1
            # Set up the rxn bounds
            for r in rxn_group[0]:
                rxn = model.reactions.get_by_id(r)
                rxn.lower_bound, rxn.upper_bound = wtopt_r2(rxn, model_clean, eps)
                # print(rxn.id + ": [" + str(rxn.lower_bound) + ", " + str(rxn.upper_bound) + "]")
            if args.biomass != 0:
                biomassrxn.lower_bound = optimise_wt(model, args.biomass)
            # rxn_1 = model.reactions.get_by_id("MAR09075")
            # print(rxn_1.id + ": [" + str(rxn_1.lower_bound) + ", " + str(rxn_1.upper_bound) + "]")

            if args.scenario == "wt":
                # SAMPLING WT
                # Now we need to run the WT sampling on the model with these bounds and objective value
                if not args.dryrun:
                    if args.fva or args.onlyfva:
                        # Run WT FVA here
                        fva_s = run_fva(model, rxns_of_interest, args.processors, args.biomass)
                        fva_results = extract_fva_results(fva_s, args.results, model)
                        write_fva(fva_results, args.outpath, "", "", "WT", fname)
                    if not args.onlyfva:
                        s = sample_time(model, args.nsamples, args.processors, args.thinning)
                        s_results = extract_results(s, args.results, model)
                        # write_sampling(s_results, args.outpath, args.model, args.nsamples, "WT", fname)
                        write_time = time.time()
                        write_sampling(s_results, args.outpath, "", "", "WT", fname)
                        print("Sampling write time: " + str(time.time() - write_time))

            if args.scenario == "ko":
                with model:
                    reduction = float(rxn_group[1])
                    print("Reducing flux to %f%% of its maximum." % (reduction*100))
                    for r in rxn_group[0]:
                        rxn = model.reactions.get_by_id(r)
                        # FVA to get maximum range for reduction, if reduction = 0 this is just a KO
                        if reduction != 0:
                            fva_s = run_fva(model, rxn, args.processors, args.biomass)
                            fva_results = fva_s.loc[r]
                            # Set all the reactions to KO to reduction * max bound
                            if fva_results[0] >= 0 and fva_results[1] > 0:  # Forward rxn
                                rxn.upper_bound = fva_results[0] + (reduction * (fva_results[1] - fva_results[0]))
                            elif fva_results[0] < 0 and fva_results[1] <= 0:  # Backward rxn
                                rxn.lower_bound = fva_results[1] + (reduction * (fva_results[0] - fva_results[1]))
                            else:  # Reversible rxn
                                rxn.lower_bound = reduction * fva_results[0]
                                rxn.upper_bound = reduction * fva_results[1]
                        else:

                            rxn.lower_bound = rxn.upper_bound = 0

                    # Re-optimise for the biomass function now that the KO reaction bounds are different
                    if args.biomass != 0:
                        biomassrxn.lower_bound = optimise_wt(model, args.biomass)
                    # rxn_1 = model.reactions.get_by_id("MAR09075")
                    # print(rxn_1.id + " KO: [" + str(rxn_1.lower_bound) + ", " + str(rxn_1.upper_bound) + "]")

                    # SAMPLING MUT
                    # Now we need to run the MUT sampling on the model with these bounds and objective value
                    if not args.dryrun:
                        if args.fva or args.onlyfva:
                            # Run MUT FVA here
                            fva_s = run_fva(model, rxns_of_interest, args.processors, args.biomass)
                            fva_results = extract_fva_results(fva_s, args.results, model)
                            write_fva(fva_results, args.outpath, "", "", "KO", fname)
                        if not args.onlyfva:
                            s = sample_time(model, args.nsamples, args.processors, args.thinning)
                            s_results = extract_results(s, args.results, model)
                            write_sampling(s_results, args.outpath, "", "", "KO", fname)
                        #write_sampling(s_results, args.outpath, args.model, args.nsamples, "KO", fname)

    # Trying to set the existing objective coeff to 0: not sure if it works yet
    # obj_rxn = model.reactions.get_by_id(str(model.objective.expression.args[0])[4:])
    # obj = {obj_rxn: 0}
    # model.objective = obj


if __name__ == '__main__':
    main(sys.argv[1:])
